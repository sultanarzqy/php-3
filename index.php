<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once("ape.php");
$sheep = new Animal('shaun');

echo "nama =". $sheep->nama . "<br>"; // "shaun"
echo "legs =".$sheep->legs . "<br>"; // 4
echo "cold blooded =".$sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "nama =". $kodok->nama . "<br>"; // "shaun"
echo "legs =".$kodok->legs . "<br>"; // 4
echo "cold blooded =".$kodok->cold_blooded. "<br>" ;
echo "jump :" .$kodok->jump. "<br><br>"; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "nama =". $sungokong->nama . "<br>"; // "shaun"
echo "legs =".$sungokong->legs . "<br>"; // 4
echo "cold blooded =".$sungokong->cold_blooded . "<br>"; // "no"
echo "Yell :" .$sungokong->yell ; // "Auooo"

?>